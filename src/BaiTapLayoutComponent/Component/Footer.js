import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <footer className="py-5 bg-dark text-center">
        <p className="text-light">Copyright © Your Website 2023</p>
      </footer>
    );
  }
}
