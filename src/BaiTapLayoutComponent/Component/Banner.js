import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div>
        <div className="my-4 bg-light text-center p-5">
          <h1 className="display-4 font-weight-bold">A Warm Welcome</h1>
          <p className="h3">
            Bootstrap utility classes are used to create this jumbotron since
            the old component has been removed from the framework. Why create
            custom CSS when you can use utilities?
          </p>
          <button className="btn btn-primary btn-lg">Call to Action</button>
        </div>
      </div>
    );
  }
}
