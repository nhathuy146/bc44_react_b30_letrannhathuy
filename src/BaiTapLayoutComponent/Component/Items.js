import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <div className="my-5 row text-center">
        <div className="col-4 mb-5">
          <div className="card" style={{ width: "18rem" }}>
            <img
              className="card-img-top"
              src="https://cdn.tgdd.vn/2021/07/CookProduct/Cach-nau-PHO-SUON-BO-SAI-GON-NUOC-NGON-DAM-DA-12-10-screenshot-1200x676.jpg"
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card" style={{ width: "18rem" }}>
            <img
              className="card-img-top"
              src="https://cdn.tgdd.vn/2021/07/CookProduct/Cach-nau-PHO-SUON-BO-SAI-GON-NUOC-NGON-DAM-DA-12-10-screenshot-1200x676.jpg"
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card" style={{ width: "18rem" }}>
            <img
              className="card-img-top"
              src="https://cdn.tgdd.vn/2021/07/CookProduct/Cach-nau-PHO-SUON-BO-SAI-GON-NUOC-NGON-DAM-DA-12-10-screenshot-1200x676.jpg"
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card" style={{ width: "18rem" }}>
            <img
              className="card-img-top"
              src="https://cdn.tgdd.vn/2021/07/CookProduct/Cach-nau-PHO-SUON-BO-SAI-GON-NUOC-NGON-DAM-DA-12-10-screenshot-1200x676.jpg"
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card" style={{ width: "18rem" }}>
            <img
              className="card-img-top"
              src="https://cdn.tgdd.vn/2021/07/CookProduct/Cach-nau-PHO-SUON-BO-SAI-GON-NUOC-NGON-DAM-DA-12-10-screenshot-1200x676.jpg"
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card" style={{ width: "18rem" }}>
            <img
              className="card-img-top"
              src="https://cdn.tgdd.vn/2021/07/CookProduct/Cach-nau-PHO-SUON-BO-SAI-GON-NUOC-NGON-DAM-DA-12-10-screenshot-1200x676.jpg"
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
