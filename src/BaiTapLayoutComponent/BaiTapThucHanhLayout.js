import React, { Component } from "react";
import Header from "./Component/Header";
import Banner from "./Component/Banner";
import Footer from "./Component/Footer";
import Items from "./Component/Items";

export default class BaoBaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="body">
          <div className="container">
            <Banner />
            <Items />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
